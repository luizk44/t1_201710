package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations {

	
	
	public double computeMean(NumbersBag<Number> bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				Number actual = iter.next();
				mean += actual.doubleValue();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(NumbersBag<Number> bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				Number actual = iter.next();
				value = actual.intValue();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin( NumbersBag<Number> bag )
	{
		int min = Integer.MAX_VALUE;
		int value;
		if ( bag != null )
		{
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext())
			{
				Number actual = iter.next();
				value = actual.intValue();
				if( value < min  )
					min = value;
			}
		}
		return min;
	}
	
	public int getPairNumbers( NumbersBag<Number> bag )
	{
		int peers = 0;
		int value;
		if ( bag != null )
		{
			Iterator<Number> iter = bag.getIterator();
			while( iter.hasNext() )
			{
				Number actual = iter.next();
				value = actual.intValue();
				if ( value % 2 == 0 )
					peers++;
			}
		}
		return peers;
	}
	
	public int getOddNumbers ( NumbersBag<Number> bag )
	{
		int impares = 0;
		int value;
		if ( bag != null )
		{
			Iterator<Number> iter = bag.getIterator();
			while( iter.hasNext() )
			{
				Number actual = iter.next();
				value = actual.intValue();
				if ( value % 2 != 0 )
					impares++;
			}
		}
		return impares;
	}
	
	public int getTotalAmount( NumbersBag<Number> bag )
	{
		int total = 0;
		int value;
		if ( bag != null )
		{
			Iterator<Number> iter = bag.getIterator();
			while( iter.hasNext() )
			{
				Number actual = iter.next();
				value = actual.intValue();
				total += value;
			}
		}
		return total;
		
	}
	
}
