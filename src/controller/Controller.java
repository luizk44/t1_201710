package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag<Number> createBag(ArrayList<Number> values){
         return new NumbersBag<Number>(values);		
	}
	
	
	public static double getMean(NumbersBag<Number> bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag<Number> bag){
		return model.getMax(bag);
	}
	
	public static double getMin(NumbersBag<Number> bag){
		return model.getMin(bag);
	}
	
	public static double getPairNumbers(NumbersBag<Number> bag){
		return model.getPairNumbers(bag);
	}
	
	public static double getTotalAmount(NumbersBag<Number> bag){
		return model.getTotalAmount(bag);
	}
	
	public static double getOddNumbers(NumbersBag<Number> bag){
		return model.getOddNumbers(bag);
	}
	
}
